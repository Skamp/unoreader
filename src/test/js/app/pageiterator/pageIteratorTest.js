/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

test("constructor, with new, should create instance", function() {
    var iterator = new unoreader.PageIterator([]);
    ok(iterator && typeof iterator === "object", "Must be an object");
    ok($.isFunction(iterator.next), "next must be a Function");
});

test("constructor, without new, should create instance", function() {
    var iterator = unoreader.PageIterator([]);
    ok(iterator && typeof iterator === "object", "Must be an object");
    ok($.isFunction(iterator.next), "next must be a Function");
});

test("next, emtpy array, returns empty array", function() {
    var iterator = new unoreader.PageIterator([]);
    ok(iterator.next(10).length === 0);
    ok(iterator.next(10).length === 0);
});

test("next, array with length smaller than pageSize, returns all elements", function() {
    var iterator = new unoreader.PageIterator(arrayLength4);
    ok(iterator.next(10).length === arrayLength4.length, "first call to iterator should return results");
    ok(iterator.next(10).length === 0, "no more results should be returned");
});

test("next, array with length equal to pageSize, returns all elements", function() {
    var iterator = new unoreader.PageIterator(arrayLength10);

    var result = iterator.next(10);
    ok(result.length === arrayLength10.length, "first call to iterator should return results");
    validateArrayContent(result, 1);

    result = iterator.next(10);
    ok(result.length === 0, "no more results should be returned");
    ok(result.length === 0, "no more results should be returned");
});

test("next, array with length exact double than pageSize, returns all elements", function() {
    var iterator = new unoreader.PageIterator(arrayLength20);
    var result = iterator.next(10);
    ok(result.length === 10, "first call to iterator should return results");
    validateArrayContent(result, 1);

    result = iterator.next(10);
    ok(result.length === 10, "second call to iterator should return results");
    validateArrayContent(result, 11);

    result = iterator.next(10);
    ok(result.length === 0, "no more results should be returned");
    ok(result.length === 0, "no more results should be returned");
});

test("next, array with length bigger than pageSize, returns all elements", function() {
    var iterator = new unoreader.PageIterator(arrayLength13);
    var result = iterator.next(10);
    ok(result.length === 10, "first call to iterator should return results");
    validateArrayContent(result, 1);

    result = iterator.next(10);
    ok(result.length === 3, "second call to iterator should return results");
    validateArrayContent(result, 11);

    result = iterator.next(10);
    ok(result.length === 0, "no more results should be returned");
    ok(result.length === 0, "no more results should be returned");
});

test("hasNext, array with length bigger than pageSize, returns true when there are elements", function() {
    var iterator = new unoreader.PageIterator(arrayLength13);
    ok(iterator.hasNext());

    iterator.next(10);
    ok(iterator.hasNext());

    iterator.next(10);
    ok(iterator.hasNext() === false);
    ok(iterator.hasNext() === false);
});

test("hasNext, empty array, returns false", function() {
    var iterator = new unoreader.PageIterator([]);
    ok(iterator.hasNext() === false);
});


function validateArrayContent(array, initial) {
    for (var i = 0; i < array.length; i++) {
        ok(array[i] === (initial + i));
    }
}

var arrayLength4 = [1, 2, 3, 4];
var arrayLength10 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
var arrayLength13 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
var arrayLength20 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];