/*
 * Schema creation script for production database.
 */
drop table if exists feed_pack;
drop table if exists feed;
drop table if exists authority;
drop table if exists favourite;
drop table if exists password_recovery;
drop table if exists user;

create table user (
    id bigint auto_increment primary key,
    username varchar(50) unique not null,
    password varchar(100) not null,
    enabled boolean not null,
    member_since datetime not null,
    last_login datetime
);

create table authority (
    id bigint auto_increment primary key,
    id_user bigint not null,
    authority varchar(50) not null,
    constraint fk_authorities_user foreign key(id_user) references user(id)
);
create unique index ix_auth_user on authority(id_user,authority);

create table feed (
    id bigint auto_increment primary key,
    name varchar(100) not null,
    url varchar(500) not null,
    channel varchar(50) not null,
    created_on datetime not null,
    id_user bigint not null,
    constraint fk_feed_user foreign key(id_user) references user(id)
);
create unique index ix_url_channel_user on feed(url,channel,id_user);

create table feed_pack (
    id bigint auto_increment primary key,
    pack varchar(50) not null,
    name varchar(100) not null,
    url varchar(500) not null,
    channel varchar(50) not null
);

create table favourite (
    id bigint auto_increment primary key,
    title varchar(200) not null,
    snippet varchar(1000),
    url varchar(500) not null,
    created_on datetime not null,
    id_user bigint not null,
    constraint fk_favourite_user foreign key(id_user) references user(id)
);
create unique index ix_url_user on favourite(url,id_user);

create table password_recovery (
    id bigint auto_increment primary key,
    token varchar(100) unique not null,
    id_user bigint not null,
    created_on datetime not null,
    constraint fk_password_recovery_user foreign key(id_user) references user(id)
);


/*
 * Test data.
 * Password is "test".
 */
insert into user (id, username, password, enabled, member_since) values (1, 'leonardo.deseta@ideasagiles.com', '1af745bdd734e82a62f8042f6ed27715ced6f81ab9a561b7675e5ed89744096d56edbaf89010cc56', true, date_sub(NOW(),interval 1 year));
insert into authority (id_user, authority) values (1, 'user');


/*
 * Feed packs.
 */
insert into feed_pack (pack, channel, name, url) values ('apple', 'apple', 'Apple Insider', 'http://www.appleinsider.com/appleinsider.rss');
insert into feed_pack (pack, channel, name, url) values ('apple', 'apple', 'iPhone Central', 'http://feeds.macworld.com/macworld/iphone');
insert into feed_pack (pack, channel, name, url) values ('apple', 'apple', 'MacDailyNews', 'http://macdailynews.com/index.php/weblog/rss_2.0');
insert into feed_pack (pack, channel, name, url) values ('apple', 'apple', 'MacRumors', 'http://www.macrumors.com/macrumors.xml');

insert into feed_pack (pack, channel, name, url) values ('art', 'art & photos', '500px', 'http://feed.500px.com/500px-best');
insert into feed_pack (pack, channel, name, url) values ('art', 'art & photos', 'Daily Flickr', 'http://www.flourish.org/news/flickr-daily-interesting.xml');
insert into feed_pack (pack, channel, name, url) values ('art', 'art & photos', 'Daily Imagery', 'http://wvs.topleftpixel.com/index_fullfeed.rdf');
insert into feed_pack (pack, channel, name, url) values ('art', 'art & photos', 'DP Review', 'http://www.dpreview.com/feeds/news/latest');

insert into feed_pack (pack, channel, name, url) values ('comedy', 'comedy', 'Dilbert', 'http://feeds.feedburner.com/DilbertDailyStrip');
insert into feed_pack (pack, channel, name, url) values ('comedy', 'comedy', 'Penny Arcade', 'http://penny-arcade.com/feed');
insert into feed_pack (pack, channel, name, url) values ('comedy', 'comedy', 'Wumo', 'http://feeds.feedburner.com/wulffmorgenthaler');
insert into feed_pack (pack, channel, name, url) values ('comedy', 'comedy', 'xkcd.com', 'http://xkcd.com/rss.xml');

insert into feed_pack (pack, channel, name, url) values ('ebooks-kindle', 'kindle ebooks', 'Amazon.com: Horror', 'http://www.amazon.com/gp/rss/new-releases/digital-text/157060011/ref=zg_bsnr_157060011_rsslink');
insert into feed_pack (pack, channel, name, url) values ('ebooks-kindle', 'kindle ebooks', 'Amazon.com: Literary Fiction', 'http://www.amazon.com/gp/rss/new-releases/digital-text/157053011/ref=zg_bsnr_157053011_rsslink');
insert into feed_pack (pack, channel, name, url) values ('ebooks-kindle', 'kindle ebooks', 'Amazon.com: Science Fiction', 'http://www.amazon.com/gp/rss/new-releases/digital-text/158591011/ref=zg_bsnr_158591011_rsslink');
insert into feed_pack (pack, channel, name, url) values ('ebooks-kindle', 'kindle ebooks', 'Amazon.com: Short Stories', 'http://www.amazon.com/gp/rss/new-releases/digital-text/157087011/ref=zg_bsnr_157087011_rsslink');
insert into feed_pack (pack, channel, name, url) values ('ebooks-kindle', 'kindle ebooks', 'Amazon.com: Top Free', 'http://www.amazon.com/gp/rss/top-free/digital-text/154606011/ref=zg_tf_154606011_rsslink');
insert into feed_pack (pack, channel, name, url) values ('ebooks-kindle', 'kindle ebooks', 'manybooks.net', 'http://manybooks.net/index.xml');
insert into feed_pack (pack, channel, name, url) values ('ebooks-kindle', 'kindle ebooks', 'OpenLibra', 'http://www.etnassoft.com/olrss/all/newest/num_items=100');

insert into feed_pack (pack, channel, name, url) values ('news-spanish', 'noticias en español', 'Clarin.com', 'http://www.clarin.com/rss');
insert into feed_pack (pack, channel, name, url) values ('news-spanish', 'noticias en español', 'CNN', 'http://cnnespanol.cnn.com/feed');
insert into feed_pack (pack, channel, name, url) values ('news-spanish', 'noticias en español', 'El País - América', 'http://ep01.epimg.net/rss/elpais/portada_america.xml');
insert into feed_pack (pack, channel, name, url) values ('news-spanish', 'noticias en español', 'La Nación', 'http://contenidos.lanacion.com.ar/herramientas/rss/origen=2');
insert into feed_pack (pack, channel, name, url) values ('news-spanish', 'noticias en español', 'Página/12', 'http://www.pagina12.com.ar/diario/rss/principal.xml');

insert into feed_pack (pack, channel, name, url) values ('news-world', 'world news', 'BBC', 'http://feeds.bbci.co.uk/news/rss.xml');
insert into feed_pack (pack, channel, name, url) values ('news-world', 'world news', 'CNN', 'http://rss.cnn.com/rss/edition.rss');
insert into feed_pack (pack, channel, name, url) values ('news-world', 'world news', 'Reddit', 'http://www.reddit.com/r/worldnews/.rss');
insert into feed_pack (pack, channel, name, url) values ('news-world', 'world news', 'Reuters', 'http://feeds.reuters.com/reuters/worldNews');
insert into feed_pack (pack, channel, name, url) values ('news-world', 'world news', 'The Guardian', 'http://feeds.guardian.co.uk/theguardian/world/rss');

insert into feed_pack (pack, channel, name, url) values ('software', 'software development', 'Coding Horror', 'http://feeds.feedburner.com/codinghorror/');
insert into feed_pack (pack, channel, name, url) values ('software', 'software development', 'InfoQ', 'http://www.infoq.com/rss/rss.action?token=rHuNq7KHMOY24D3tmVGBlsaAsYzb7R6r');
insert into feed_pack (pack, channel, name, url) values ('software', 'software development', 'Java Lobby', 'http://feeds.dzone.com/javalobby/frontpage');
insert into feed_pack (pack, channel, name, url) values ('software', 'software development', 'Signal vs. Noise', 'http://feeds.feedburner.com/37signals/beMH');
insert into feed_pack (pack, channel, name, url) values ('software', 'software development', 'The Server Side', 'http://www.theserverside.com/rss/theserverside-rss2.xml');

insert into feed_pack (pack, channel, name, url) values ('sports', 'sports', 'BBC Sport', 'http://feeds.bbci.co.uk/sport/0/rss.xml');
insert into feed_pack (pack, channel, name, url) values ('sports', 'sports', 'ESPN', 'http://sports-ak.espn.go.com/espn/rss/news');
insert into feed_pack (pack, channel, name, url) values ('sports', 'sports', 'Fox Sports', 'http://feeds.pheedo.com/feedout/syndicatedContent_categoryId_0');
insert into feed_pack (pack, channel, name, url) values ('sports', 'sports', 'Sporting News', 'http://aol.sportingnews.com/rss');

insert into feed_pack (pack, channel, name, url) values ('technology', 'technology', 'Ars Technica', 'http://feeds.arstechnica.com/arstechnica/index');
insert into feed_pack (pack, channel, name, url) values ('technology', 'technology', 'Engadget', 'http://www.engadget.com/rss.xml');
insert into feed_pack (pack, channel, name, url) values ('technology', 'technology', 'Slashdot', 'http://slashdot.org/index.rss');
insert into feed_pack (pack, channel, name, url) values ('technology', 'technology', 'TechCrunch', 'http://feeds.feedburner.com/Techcrunch');
insert into feed_pack (pack, channel, name, url) values ('technology', 'technology', 'Wired', 'http://feeds.wired.com/wired/index');

insert into feed_pack (pack, channel, name, url) values ('videogames', 'videogames', 'Destructoid', 'http://www.destructoid.com/?mode=atom');
insert into feed_pack (pack, channel, name, url) values ('videogames', 'videogames', 'Gamespot', 'http://www.gamespot.com/rss/game_updates.php');
insert into feed_pack (pack, channel, name, url) values ('videogames', 'videogames', 'IGN Games', 'http://feeds.ign.com/ign/games-articles?format=xml');
insert into feed_pack (pack, channel, name, url) values ('videogames', 'videogames', 'Joystiq', 'http://www.joystiq.com/rss.xml');
insert into feed_pack (pack, channel, name, url) values ('videogames', 'videogames', 'Polygon', 'http://www.polygon.com/rss/index.xml');
insert into feed_pack (pack, channel, name, url) values ('videogames', 'videogames', 'Rock, Paper, Shotgun', 'http://feeds.feedburner.com/RockPaperShotgun');
