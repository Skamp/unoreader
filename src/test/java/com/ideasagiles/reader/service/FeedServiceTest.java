/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.service;

import com.ideasagiles.reader.AbstractDatabaseTest;
import com.ideasagiles.reader.SecurityTestUtils;
import com.ideasagiles.reader.domain.Feed;
import java.util.List;
import javax.validation.ConstraintViolationException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.Assert.*;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class FeedServiceTest extends AbstractDatabaseTest {

    @Autowired
    private FeedService feedService;

    @Test
    public void save_validFeedWithChannels_savesTheFeedAndChannels() {
        SecurityTestUtils.loginTestUser();
        Feed feed = new Feed();
        feed.setName("Test Feed");
        feed.setUrl("http://www.dosideas.com/noticias.feed?type=rss");
        feed.setChannel("  Test Channel  ");

        feedService.save(feed);

        assertNotNull(feed.getId());
        assertNotNull(feed.getCreatedOn());
        assertEquals(feed.getChannel().trim().toLowerCase(), feed.getChannel());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void save_feedExistsOnChannel_throwsException() {
        SecurityTestUtils.loginTestUser();
        Feed feed = new Feed();
        feed.setName("Dos Ideas");
        feed.setUrl("http://www.dosideas.com/noticias.feed?type=rss");
        feed.setChannel("software development");
        feedService.save(feed);
    }

    @Test
    public void save_feedExistsOnDifferentChannel_throwsException() {
        SecurityTestUtils.loginTestUser();
        Feed feed = new Feed();
        feed.setName("Dos Ideas");
        feed.setUrl("http://www.dosideas.com/noticias.feed?type=rss");
        feed.setChannel("technology");
        feedService.save(feed);
        assertNotNull(feed.getId());
    }

    @Test(expected = ConstraintViolationException.class)
    public void save_InvalidUrl_throwsException() {
        SecurityTestUtils.loginTestUser();
        Feed feed = new Feed();
        feed.setName("Dos Ideas");
        feed.setUrl("invalid url");
        feed.setChannel("software development");
        feedService.save(feed);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void save_userNotAuthenticated_throwsSecurityException() {
        feedService.save(new Feed());
    }

    @Test
    public void findAll_userIsLoggedIn_returnsFeedsFromUser() {
        SecurityTestUtils.loginTestUser();
        List<Feed> feeds = feedService.findAll();
        assertNotNull(feeds);
        assertTrue(feeds.size() > 1);
        for (Feed feed : feeds) {
            assertEquals(SecurityTestUtils.USER_DEFAULT, feed.getUser().getUsername());
        }
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void findAll_userNotAuthenticated_throwsSecurityException() {
        SecurityTestUtils.logout();
        feedService.findAll();
    }

}
