/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.service;

import com.ideasagiles.reader.AbstractDatabaseTest;
import com.ideasagiles.reader.SecurityTestUtils;
import com.ideasagiles.reader.domain.Favourite;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.Assert.*;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;

public class FavouriteServiceTest extends AbstractDatabaseTest {

    @Autowired
    private FavouriteService favouriteService;

    @Test
    public void save_validFavourite_savesFavourite() {
        SecurityTestUtils.loginTestUser();

        Favourite favourite = new Favourite();
        favourite.setTitle("A title");
        favourite.setUrl("http://www.unoreader.com");
        favourite.setSnippet("A brief content snippet.");
        favouriteService.save(favourite);

        assertNotNull(favourite.getId());
        assertNotNull(favourite.getCreatedOn());
        assertNotNull(favourite.getUser());
        assertEquals(SecurityTestUtils.USER_DEFAULT, favourite.getUser().getUsername());
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void save_userIsNotLoggedIn_thorwsSecurityException() {
        favouriteService.save(new Favourite());
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void delete_userIsNotLoggedIn_thorwsSecurityException() {
        favouriteService.delete(1L);
    }

    @Test(expected = AccessDeniedException.class)
    public void delete_idDoesNotExist_thorwsSecurityException() {
        SecurityTestUtils.loginTestUser();
        favouriteService.delete(Long.MIN_VALUE);
    }

}
