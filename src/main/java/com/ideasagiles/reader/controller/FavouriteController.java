/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.controller;

import com.ideasagiles.reader.domain.Favourite;
import com.ideasagiles.reader.service.FavouriteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class FavouriteController {

    @Autowired
    private FavouriteService favouriteService;

    @RequestMapping(value = "/favourite", method = RequestMethod.GET)
    public @ResponseBody List<Favourite> findAll() {
        return favouriteService.findAll();
    }

    @RequestMapping(value = "/favourite", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void save(@RequestBody Favourite favourite) {
        favouriteService.save(favourite);
    }

    @RequestMapping(value = "/favourite/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        favouriteService.delete(id);
    }
}
