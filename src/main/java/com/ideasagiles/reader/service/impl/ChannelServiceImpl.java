/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.service.impl;

import com.ideasagiles.reader.domain.Feed;
import com.ideasagiles.reader.domain.User;
import com.ideasagiles.reader.repository.FeedRepository;
import com.ideasagiles.reader.service.ChannelService;
import com.ideasagiles.reader.service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ChannelServiceImpl implements ChannelService {

    @Autowired
    private UserService userService;
    @Autowired
    private FeedRepository feedRepository;

    @PreAuthorize("isAuthenticated()")
    @Override
    public void rename(String oldChannelName, String newChannelName) {
        User user = userService.findLoggedInUser();
        List<Feed> feeds = feedRepository.findByChannelAndUser(oldChannelName, user);
        for (Feed feed : feeds) {
            feed.setChannel(newChannelName);
        }
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public void deleteChannelAndFeeds(String channel) {
        User user = userService.findLoggedInUser();
        List<Feed> feeds = feedRepository.findByChannelAndUser(channel, user);
        feedRepository.delete(feeds);
    }
}
