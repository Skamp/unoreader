/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.service;

public interface ChannelService {
    /**
     * Renames a channel of the current logged in user.
     * @param oldChannelName the current channel name.
     * @param newChannelName the new channel name.
     */
    void rename(String oldChannelName, String newChannelName);

    /**
     * Deletes all feeds from the given channel.
     * @param channel the channel to delete.
     */
    void deleteChannelAndFeeds(String channel);
}
