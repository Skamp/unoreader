/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
unoreader.service.favourite = (function() {

    function findAll(successCallback, errorCallback) {
        var url = unoreader.service.baseURI + "/favourite";
        $.getJSON(url)
                .done(successCallback)
                .fail(errorCallback);
    }

    function save(favourite, successCallback, errorCallback) {
        var url = unoreader.service.baseURI + "/favourite";
        unoreader.service.postJSON({
            url: url,
            data: favourite,
            success: successCallback,
            error: errorCallback
        });
    }

    function deleteFavourite(id, successCallback, errorCallback) {
        var url = unoreader.service.baseURI + "/favourite/" + id;
        unoreader.service.deleteJSON({
            url: url,
            success: successCallback,
            error: errorCallback
        });
    }

    return {
        findAll: findAll,
        deleteFavourite: deleteFavourite,
        save: save
    };
})();