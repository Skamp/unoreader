/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var page = (function() {
    function init() {
        $("#resetForm input[name='newPassword1']").val("");
        $("#resetForm input[name='newPassword2']").val("");
        $("#resetForm").focus();
        $("#resetForm").on("submit", submitForm);
    }

    function submitForm() {
        var token = $("#resetForm input[name='token']").val();
        var newPassword1 = $("#resetForm input[name='newPassword1']").val();
        var newPassword2 = $("#resetForm input[name='newPassword2']").val();
        var resetPassword;

        if (validatePassword(newPassword1, newPassword2)) {
            resetPassword = {
                token: token,
                password: newPassword1
            };
            unoreader.service.passwordRecovery.resetPassword(resetPassword, saveSuccess, saveError);
        }
        return false;
    }

    function validatePassword(password1, password2) {
        if (password1 !== password2) {
            renderMessage({
                cssClass: "alert-error",
                title: "Check your password.",
                text: "Please check that both passwords are identical."
            });
            return false;
        }
        return true;
    }

    function saveSuccess(result) {
        renderMessage({
            cssClass: "alert-success",
            title: "Changes saved. Please login again with your new password."
        });
    }

    function saveError(result) {
        renderMessage({
            cssClass: "alert-error",
            title: "The token is not valid.",
            text: "Please try reseting your password again."
        });
    }

    function renderMessage(message) {
        $("#infoArea").html($("#errorMessageTemplate").render(message));
    }

    return {
        init: init
    };
})();

$(document).ready(function() {
    page.init();
});