<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="container">
    <div class="row">
        <div class="span6 offset3">
            <div class="ur-lightbox ur-extra-margin">
                <div class="ur-header"><h3>Sign in to start</h3></div>
                <form id="loginForm" method="POST" action="<c:url value="/j_spring_security_check"/>">
                    <div class="ur-content ur-extra-padding">
                        <c:if test="${sessionScope.SPRING_SECURITY_LAST_EXCEPTION != null}" >
                            <div class="alert alert-error">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <h4>Invalid username of password</h4>
                                Please check your login information and try again.
                            </div>
                            <c:remove var="SPRING_SECURITY_LAST_EXCEPTION" scope="session" />
                        </c:if>
                        <input type="hidden" name="_spring_security_remember_me" value="on" />
                        <label>Email</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-envelope"></i></span>
                            <input type="email" class="input-xlarge" name="j_username" placeholder="Your email address..." required="required" maxlength="50" />
                        </div>
                        <label>Password</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-key"></i></span>
                            <input type="password" class="input-xlarge" name="j_password" placeholder="Your password..." required="required" pattern=".{4,25}" />
                        </div>
                        <p>
                            <a href="<c:url value="/passwordResetRequest.html"/>">Forgot your password?</a>
                        </p>
                    </div>
                    <div class="ur-actions">
                        <input class="btn btn-success" type="submit" value="Sign In"/>
                        <a class="btn" href="<c:url value="/"/>">Cancel</a>
                        <div class="pull-right">
                            <a class="btn btn-small" href="<c:url value="/signup.html"/>">Create Account</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>